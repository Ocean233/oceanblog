# pgsql常用命令

## 给schema下的所有表赋权

```
select 'grant all on table ' || schemaname || '.' || tablename || ' to jyzsuser;' 
from pg_tables where schemaname = 'haigang'
```

## 创建schema

```sql
CREATE SCHEMA schema_name AUTHORIZATION user_name;创建一个名为schema_name的schema，他的owner是user_name
```

## 创建用户

```sql
create user asher encrypted password '123456';
```

## 授予普通权限

```sql
GRANT ALL PRIVILEGES ON DATABASE aaa TO aaadev;
grant all on SCHEMA aaa to aaadev;
GRANT ALL PRIVILEGES ON all tables in schema aaa TO aaadev;

使用asher用户配置schema的usage权限给app1和app2用户
grant usage on schema asher to app1,app2;


使用asher用户配置当前所有表的select权限
grant select on all tables in schema asher to app1,app2;


使用asher用户配置当前所有表的update权限
grant update on all tables in schema asher to app1;
```

## 配置默认权限

```sql
使用asher用户配置新增表的默认权限
alter default privileges in schema asher          grant select on tables to app1,app2;

alter default privileges in schema asher          grant update on tables to app1;
```



## 锁表查询

```
查询运行的语句
select pid,backend_start,state,query from pg_stat_activity where state ='active';
select pg_terminate_backend(58625);

查询锁表语句
select pid, state, usename, query, query_start 
from pg_stat_activity 
where pid in (
  select pid from pg_locks l 
  join pg_class t on l.relation = t.oid 
  and t.relkind = 'r' 
  
);

SELECT pg_cancel_backend(652436);
```

## 查询执行计划

```
sql语句分析
explain (ANALYZE true, buffers true)

关闭nestloop
set enable_nestloop=false;
```
## 查询正在运行的sql

```
select state,query from pg_stat_activity
```

## 表空间优化

```
VACUUM   执行效率高，可并行执行。重新规划表物理空间
VACUUM FULL    执行效率低，排他锁，释放表物理空间
vacuum analyse ads_jyzs_stm_sale_storegoods_cd
```
## AO表 压缩

```
CREATE TABLE youke.ads_jyzs_stm_inv_storegoods_td (
	id varchar NULL,
)
WITH (
	appendonly=true,
	orientation=row,
	compresstype=zstd,
	compresslevel=6
);
```

## 长sql查询
​	事项 : 对pg_pasobi(用户postgres)和gp_pasobi(用户gpadmin)分别增加长sql监控;
​	每5分钟轮巡一次;
​	如果有返回结果,在钉钉中报警. 长sql查询语句如下

```
 select  usename as "用户",now()-state_change "已执行时长",state "状态",query "语句" 
 from pg_stat_activity where now()-state_change>'00:05:00' and state  ='active'  order by state_change;
```
## 创建分区表

```
CREATE TABLE youke.ads_jyzs_stm_sale_storegoods_cd_copy (
	id varchar NULL,
	datedday date NULL,
	store_gid int4 NULL
)
WITH (
	appendonly=true,
	orientation=row
) partition by range (datedday) 
(
	partition pm_ start ('2020-07-01'::date) end ('2021-08-01'::date) every ('1 month'::interval),
	partition pd_ start ('2020-08-01'::date) end ('2021-08-31'::date) every ('1 day'::interval),
	DEFAULT PARTITION pdefault
);

appendonly=false

```
## postgis查询两点距离

```
select st_distance(st_transform(st_geometryfromtext(CONCAT('POINT(',user_longitude,' ',user_latitude,')'),4326),4527),st_transform(st_geometryfromtext(CONCAT('POINT(',store_longitude,' ',store_latitude,')'),4326),4527))::int;
```

## GP使用手册

```
su - gpadmin

gpstart #正常启动

gpstop #正常关闭

gpstop -M fast #快速关闭

gpstop –r #重启

gpstop –u #重新加载配置文件
```

## 登陆与退出Greenplum

```
正常登陆
psql gpdb
psql -d gpdb -h gphostm -p 5432 -U gpadmin

使用utility方式
PGOPTIONS="-c gp_session_role=utility" psql -h -d dbname hostname -p port

退出
在psql命令行执行\q
```
## 参数查询

```
psql -c 'SHOW ALL;' -d gpdb
gpconfig --show max_connections
```

## 创建数据库

```
createdb -h localhost -p 5432 dhdw
```

## 创建GP文件系统

```
文件系统名
gpfsdw

子节点，视segment数创建目录
mkdir -p /gpfsdw/seg1

mkdir -p /gpfsdw/seg2

chown -R gpadmin:gpadmin /gpfsdw

主节点

mkdir -p /gpfsdw/master

chown -R gpadmin:gpadmin /gpfsdw

gpfilespace -o gpfilespace_config

gpfilespace -c gpfilespace_config
```

## 创建GP表空间

```
psql gpdb

create tablespace TBS_DW_DATA filespace gpfsdw;

SET default_tablespace = TBS_DW_DATA;
```

## 删除GP数据库

```
gpdeletesystem -d /gpmaster/gpseg-1 -f
```

## 查看segment配置

```
select * from gp_segment_configuration;
```

## 文件系统

```
select * from pg_filespace_entry;
```

## 磁盘、数据库空间

```
SELECT * FROM gp_toolkit.gp_disk_free ORDER BY dfsegment;

SELECT * FROM gp_toolkit.gp_size_of_database ORDER BY sodddatname;
```

## 日志

```
SELECT * FROM gp_toolkit.__gp_log_master_ext;

SELECT * FROM gp_toolkit.__gp_log_segment_ext;
```

## 表描述

```
/d+ <tablename>
```

## 表数据分布

```
SELECT gp_segment_id, count(*) FROM <table_name> GROUP BY gp_segment_id;
```

## 表占用空间

```
SELECT relname as name, sotdsize/1024/1024 as size_MB, sotdtoastsize as toast, sotdadditionalsize as other
FROM gp_toolkit.gp_size_of_table_disk as sotd, pg_class
WHERE sotd.sotdoid = pg_class.oid ORDER BY relname;
```

## 索引占用空间

```
SELECT soisize/1024/1024 as size_MB, relname as indexname
FROM pg_class, gp_toolkit.gp_size_of_index
WHERE pg_class.oid = gp_size_of_index.soioid
AND pg_class.relkind='i';
```

## OBJECT的操作统计

```
SELECT schemaname as schema, objname as table, usename as role, actionname as action, subtype as type, statime as time
FROM pg_stat_operations
WHERE objname = '<name>';
```
## 锁

```
SELECT locktype, database, c.relname, l.relation, l.transactionid, l.transaction, l.pid, l.mode, l.granted, a.current_query
FROM pg_locks l, pg_class c, pg_stat_activity a
WHERE l.relation=c.oid
AND l.pid=a.procpid
ORDER BY c.relname;
```
## 队列

```
SELECT * FROM pg_resqueue_status;
```

## 加载（LOAD）数据到Greenplum数据库

```
gpfdist外部表
启动服务

gpfdist -d /share/txt -p 8081 –l /share/txt/gpfdist.log &

创建外部表，分隔符为’/t’

drop EXTERNAL TABLE TD_APP_LOG_BUYER;

CREATE EXTERNAL TABLE TD_APP_LOG_BUYER (
IP text,
ACCESSTIME text,
REQMETHOD text,
URL text,
STATUSCODE int,
REF text,
name text,
VID text)
LOCATION ('gpfdist://gphostm:8081/xxx.txt')
FORMAT 'TEXT' (DELIMITER E'/t' FILL MISSING FIELDS) 
SEGMENT REJECT LIMIT 1 percent;

创建普通表
create table test select * from TD_APP_LOG_BUYER;

索引
CREATE INDEX idx_test ON test USING bitmap (ip);

查询数据
select ip , count(*) from test group by ip order by count(*);

gpload
创建控制文件
加载数据
gpload -f my_load.yml
copy
COPY country FROM '/data/gpdb/country_data'
WITH DELIMITER '|' LOG ERRORS INTO err_country
SEGMENT REJECT LIMIT 10 ROWS;
```

## 从Greenplum数据库卸载（UNLOAD）数据

```
gpfdist外部表
# 创建可写外部表
CREATE WRITABLE EXTERNAL TABLE unload_expenses
( LIKE expenses )
LOCATION ('gpfdist://etlhost-1:8081/expenses1.out',
'gpfdist://etlhost-2:8081/expenses2.out')
FORMAT 'TEXT' (DELIMITER ',')
DISTRIBUTED BY (exp_id);

写权限
GRANT INSERT ON writable_ext_table TO <name>;

写数据
INSERT INTO writable_ext_table SELECT * FROM regular_table;

copy
COPY (SELECT * FROM country WHERE country_name LIKE 'A%') TO '/home/gpadmin/a_list_countries.out';
```

## 执行sql文件

```
psql gpdbname –f yoursqlfile.sql

或者psql登陆后执行
\i yoursqlfile.sql
```


```
去重
select userid, rank,count(sold_date) count_rank from (
select user_id,sold_date,sold_rank,rank_c_day,sold_rank- rank_c_day rank from ly_temp temp_wang_20200331_user_all a
join ly_temp.temp_wang_20200331_dim_date b on a.sold_date = b.c_day
- - where user_id = 2770186 order by 2
group by user_id,rank having count(sold_date) >= 15 order by 3 desc
```

