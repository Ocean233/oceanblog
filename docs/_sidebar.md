<!-- _sidebar.md -->

* 工作
  * [命令-pgsql](/工作/pgsql常用命令.md)
  * [命令-linux](/工作/pgsql常用命令.md)
* 产品
  * 经营助手
    * [v2.2](/工作/pgsql常用命令.md)
    * [v2.3](/工作/pgsql常用命令.md)
  * 智能绩效
    * [v1.0](/工作/pgsql常用命令.md)
* 学习
  * [GreenPlum](/工作/pgsql常用命令.md)
  * [Docker](/学习/docker常用命令.md)
  * [docsify](/学习/docsify配置.md)
* 生活
  * [小知识](/生活/偏方.md)

